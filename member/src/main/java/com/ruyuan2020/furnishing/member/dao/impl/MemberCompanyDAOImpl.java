package com.ruyuan2020.furnishing.member.dao.impl;

import com.ruyuan2020.furnishing.common.dao.impl.BaseDAOImpl;
import com.ruyuan2020.furnishing.member.dao.MemberCompanyDAO;
import com.ruyuan2020.furnishing.member.domain.MemberCompanyDO;
import com.ruyuan2020.furnishing.member.mapper.MemberCompanyMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MemberCompanyDAOImpl extends BaseDAOImpl<MemberCompanyMapper, MemberCompanyDO> implements MemberCompanyDAO {

    @Override
    public void updateBidQuantity(Long memberId) {
        mapper.updateBidQuantity(memberId);
    }

    @Override
    public void updateSignedQuantity(Long memberId) {
        mapper.updateSignedQuantity(memberId);
    }

    @Override
    public void updateCommentQuantity(Long memberId) {
        mapper.updateCommentQuantity(memberId);
    }
}
