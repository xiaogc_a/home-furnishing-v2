package com.ruyuan2020.furnishing.member.dao.impl;

import com.ruyuan2020.furnishing.common.dao.impl.BaseDAOImpl;
import com.ruyuan2020.furnishing.member.dao.MemberDAO;
import com.ruyuan2020.furnishing.member.domain.MemberDO;
import com.ruyuan2020.furnishing.member.mapper.MemberMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDAOImpl extends BaseDAOImpl<MemberMapper, MemberDO> implements MemberDAO {


}
