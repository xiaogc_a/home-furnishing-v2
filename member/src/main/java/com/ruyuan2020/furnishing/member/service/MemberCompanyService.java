package com.ruyuan2020.furnishing.member.service;

public interface MemberCompanyService {

    void informBidCompletedEvent(Long memberId);

    void informSignedEvent(Long memberId);

    void informCommentEvent(Long memberId);
}
