package com.ruyuan2020.common.exception;

public class NotFoundException extends BaseException {

	private static final long serialVersionUID = 1L;

    public NotFoundException() {
        super(null, null);
    }
}
