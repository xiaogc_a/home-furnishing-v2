package com.ruyuan2020.furnishing.payment.dao;

import com.ruyuan2020.furnishing.common.dao.BaseDAO;
import com.ruyuan2020.furnishing.payment.domain.PaymentTransactionDO;

import java.util.Optional;

public interface PaymentTransactionDAO extends BaseDAO<PaymentTransactionDO> {

    Optional<PaymentTransactionDO> getByTradeNo(String tradeNo);
}
