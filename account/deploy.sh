chmod 755 ./deploy.sh
jps
pid=$(jps | grep account.jar | awk '{split($1,arr," ");print arr[1]}')
if [ -n "${pid}" ]; then
  echo "kill ${pid}"
  kill -9 ${pid}
fi
rm -rf ./app.log
JAVA_OPTS="-Xms5g -Xmx5g -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=35 -XX:G1MaxNewSizePercent=50"
#JAVA_OPTS=""
sh -c "nohup java ${JAVA_OPTS} -jar ./target/account.jar >app.log 2>&1 &"