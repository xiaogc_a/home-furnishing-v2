package com.ruyuan2020.furnishing.account.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruyuan2020.common.domain.BaseDO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@TableName("ry_account_log")
public class AccountLogDO extends BaseDO {

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 类型（金币、积分）
     */
    private String type;

    /**
     * 数量
     */
    private BigDecimal number;

    /**
     * 日志
     */
    private String log;

    /**
     * 交易流水号
     */
    private String tradeNo;
}
