package com.ruyuan2020.furnishing.account.dao.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.account.dao.AccountDAO;
import com.ruyuan2020.furnishing.account.domain.AccountDO;
import com.ruyuan2020.furnishing.account.mapper.AccountMapper;
import com.ruyuan2020.furnishing.common.dao.impl.BaseDAOImpl;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public class AccountDAOImpl extends BaseDAOImpl<AccountMapper, AccountDO> implements AccountDAO {

    @Override
    public BigDecimal getGoldById(Long memberId) {
        LambdaQueryWrapper<AccountDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(AccountDO::getGold);
        queryWrapper.eq(AccountDO::getMemberId, memberId);
        return Optional.ofNullable(mapper.selectOne(queryWrapper)).map(AccountDO::getGold).orElseThrow(() -> new BusinessException("会员信息不存在"));
    }

    @Override
    public void addGoldAndPoint(Long memberId, BigDecimal gold, BigDecimal point) {
        mapper.addGoldAndPoint(memberId, gold, point);
    }

    @Override
    public void freezeGold(Long memberId, BigDecimal gold) {
        mapper.freezeGold(memberId, gold);
    }

    @Override
    public void subtractFreezeGold(Long memberId, BigDecimal gold) {
        mapper.subtractFreezeGold(memberId, gold);
    }

    @Override
    public void subtractGold(Long memberId, BigDecimal gold) {
        mapper.subtractGold(memberId, gold);
    }

}
