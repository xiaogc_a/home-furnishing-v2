package com.ruyuan2020.furnishing.account.service;


import com.ruyuan2020.furnishing.account.domain.AccountDTO;
import com.ruyuan2020.furnishing.account.domain.GoldOperationRequestDTO;

public interface AccountService {

    void createAccount(AccountDTO accountDTO);

    void payGold(GoldOperationRequestDTO goldOperationRequestDTO);

    void addGold(GoldOperationRequestDTO goldOperationRequestDTO);

    void freezeGold(GoldOperationRequestDTO goldOperationRequestDTO);

    void payFreezeGold(GoldOperationRequestDTO goldOperationRequestDTO);
}
