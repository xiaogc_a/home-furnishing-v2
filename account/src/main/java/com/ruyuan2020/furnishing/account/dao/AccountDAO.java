package com.ruyuan2020.furnishing.account.dao;

import com.ruyuan2020.furnishing.account.domain.AccountDO;
import com.ruyuan2020.furnishing.common.dao.BaseDAO;

import java.math.BigDecimal;

public interface AccountDAO extends BaseDAO<AccountDO> {

    BigDecimal getGoldById(Long memberId);

    void addGoldAndPoint(Long memberId, BigDecimal gold, BigDecimal point);

    void freezeGold(Long memberId, BigDecimal gold);

    void subtractFreezeGold(Long memberId, BigDecimal gold);

    void subtractGold(Long memberId, BigDecimal gold);
}
