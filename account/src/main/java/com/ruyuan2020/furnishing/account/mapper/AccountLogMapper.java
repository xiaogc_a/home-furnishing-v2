package com.ruyuan2020.furnishing.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan2020.furnishing.account.domain.AccountLogDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AccountLogMapper extends BaseMapper<AccountLogDO> {
}
