package com.ruyuan2020.furnishing.account.api.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.account.api.AccountApi;
import com.ruyuan2020.furnishing.account.domain.AccountDTO;
import com.ruyuan2020.furnishing.account.domain.GoldOperationRequestDTO;
import com.ruyuan2020.furnishing.account.service.AccountService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService(version = "1.0.0", interfaceClass = AccountApi.class)
public class AccountImpl implements AccountApi {

    @Autowired
    private AccountService accountService;

    @Override
//    @SentinelResource(value = "AccountApi:createAccount", blockHandler = "createAccount")
    public void createAccount(AccountDTO accountDTO) {
        accountService.createAccount(accountDTO);
    }

    public void createAccount(AccountDTO accountDTO, BlockException e) {
        throw new BusinessException("请求过于频繁，请稍后再试");
    }

    @Override
    public void addGold(GoldOperationRequestDTO goldOperationRequestDTO) throws BusinessException {
        accountService.addGold(goldOperationRequestDTO);
    }

    @Override
    public void payGold(GoldOperationRequestDTO goldOperationRequestDTO) throws BusinessException {
        accountService.payGold(goldOperationRequestDTO);
    }

    @Override
    public void freezeGold(GoldOperationRequestDTO goldOperationRequestDTO) throws BusinessException {
        accountService.freezeGold(goldOperationRequestDTO);
    }

    @Override
    public void payFreezeGold(GoldOperationRequestDTO goldOperationRequestDTO) throws BusinessException {
        accountService.payFreezeGold(goldOperationRequestDTO);
    }
}
