package com.ruyuan2020.furnishing.tender.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruyuan2020.common.domain.BaseDO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@TableName("ry_tender")
@Getter
@Setter
public class TenderDO extends BaseDO {

    private Long categoryId;

    private BigDecimal budgetAmount;

    private BigDecimal trustAmount;

    private BigDecimal costAmount;

    private Boolean payFlag;

    private Long memberId;

    private String contact;

    private String mobile;

    private Long cityId;

    private Long areaId;

    private String addr;

    private String title;

    private String content;

    private String photo;

    private String status;

    private Integer biddingMaxCount;
}
