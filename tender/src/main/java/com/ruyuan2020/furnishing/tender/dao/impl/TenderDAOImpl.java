package com.ruyuan2020.furnishing.tender.dao.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ruyuan2020.common.util.DateTimeUtils;
import com.ruyuan2020.furnishing.common.dao.impl.BaseDAOImpl;
import com.ruyuan2020.furnishing.tender.dao.TenderDAO;
import com.ruyuan2020.furnishing.tender.domain.BiddingDO;
import com.ruyuan2020.furnishing.tender.domain.TenderDO;
import com.ruyuan2020.furnishing.tender.mapper.TenderMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class TenderDAOImpl extends BaseDAOImpl<TenderMapper, TenderDO> implements TenderDAO {

    @Override
    public String getStatus(Long id) {
        LambdaQueryWrapper<TenderDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(TenderDO::getStatus);
        queryWrapper.eq(TenderDO::getId, id);
        return mapper.selectOne(queryWrapper).getStatus();
    }

    @Override
    public void updatePay(Long id) {
        LambdaUpdateWrapper<TenderDO> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TenderDO::getPayFlag, true);
        updateWrapper.eq(TenderDO::getId, id);
        mapper.update(null, updateWrapper);
    }

    @Override
    public Optional<TenderDO> getTrustInfoById(Long id) {
        LambdaQueryWrapper<TenderDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(TenderDO::getId, TenderDO::getStatus, TenderDO::getPayFlag, TenderDO::getMemberId, TenderDO::getTrustAmount);
        queryWrapper.eq(TenderDO::getId, id);
        return Optional.ofNullable(mapper.selectOne(queryWrapper));
    }

    @Override
    public Optional<TenderDO> getBiddingInfoById(Long id) {
        LambdaQueryWrapper<TenderDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(TenderDO::getId, TenderDO::getStatus, TenderDO::getMemberId, TenderDO::getCostAmount, TenderDO::getBiddingMaxCount);
        queryWrapper.eq(TenderDO::getId, id);
        return Optional.ofNullable(mapper.selectOne(queryWrapper));
    }

    @Override
    public Optional<TenderDO> getSigningInfoById(Long id) {
        LambdaQueryWrapper<TenderDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(TenderDO::getId, TenderDO::getStatus, TenderDO::getMemberId);
        queryWrapper.eq(TenderDO::getId, id);
        return Optional.ofNullable(mapper.selectOne(queryWrapper));
    }

    @Override
    public void updateStatus(Long id, String status) {
        LambdaUpdateWrapper<TenderDO> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TenderDO::getStatus, status);
        updateWrapper.eq(TenderDO::getId, id);
        mapper.update(null, updateWrapper);
    }

    @Override
    public Optional<TenderDO> getCompletionInfoById(Long id) {
        return getSigningInfoById(id);
    }
}
