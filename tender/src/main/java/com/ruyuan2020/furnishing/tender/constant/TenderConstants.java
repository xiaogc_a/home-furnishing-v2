package com.ruyuan2020.furnishing.tender.constant;

import java.math.BigDecimal;

public class TenderConstants {

    public static final BigDecimal DEFAULT_COST = BigDecimal.valueOf(50);

    public static final Integer DEFAULT_BIDDING_MAX_COUNT = 6;
}
