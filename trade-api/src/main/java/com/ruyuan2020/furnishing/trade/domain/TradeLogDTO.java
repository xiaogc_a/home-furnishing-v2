package com.ruyuan2020.furnishing.trade.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TradeLogDTO extends BaseDomain {

    private Long id;

    private Long memberId;

    private String paymentType;

    private BigDecimal gold;

    private BigDecimal amount;

    private String status;

    private Long tenderId;
}
