package com.ruyuan2020.furnishing.thirdPay.service.impl;

import com.ruyuan2020.furnishing.thirdPay.domain.PayRequest;
import com.ruyuan2020.furnishing.thirdPay.service.ThirdPayService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;

@Service
public class ThirdPayServiceImpl implements ThirdPayService {

    @Value("${mock.wait-time-start}")
    private final Integer waitTimeStart = 50;

    @Value("${mock.wait-time-end}")
    private final Integer waitTimeEnd = 100;

    @Override
    public String sdkExecute(PayRequest payRequest) {
        try {
            SecureRandom secureRandom = new SecureRandom();
            int waitTime = secureRandom.nextInt(waitTimeEnd - waitTimeStart + 1) + waitTimeStart;
            Thread.sleep(waitTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return payRequest.getTradeNo();
    }
}
