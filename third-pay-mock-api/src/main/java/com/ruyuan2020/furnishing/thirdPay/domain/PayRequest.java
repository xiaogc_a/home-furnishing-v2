package com.ruyuan2020.furnishing.thirdPay.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PayRequest extends BaseDomain {

    private String tradeNo;

    private BigDecimal amount;
}
