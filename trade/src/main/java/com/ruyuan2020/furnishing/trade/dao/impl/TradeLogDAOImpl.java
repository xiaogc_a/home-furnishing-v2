package com.ruyuan2020.furnishing.trade.dao.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ruyuan2020.common.domain.BasePage;
import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.common.dao.impl.BaseDAOImpl;
import com.ruyuan2020.furnishing.trade.dao.TradeLogDAO;
import com.ruyuan2020.furnishing.trade.domian.TradeLogDO;
import com.ruyuan2020.furnishing.trade.domian.TradeQuery;
import com.ruyuan2020.furnishing.trade.mapper.TradeLogMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class TradeLogDAOImpl extends BaseDAOImpl<TradeLogMapper, TradeLogDO> implements TradeLogDAO {
    @Override
    public TradeLogDO getByTradeNo(String tradeNo) {
        LambdaQueryWrapper<TradeLogDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TradeLogDO::getTradeNo, tradeNo);
        return Optional.ofNullable(mapper.selectOne(queryWrapper)).orElseThrow(() -> new BusinessException("交易信息不存在"));
    }

    @Override
    public void updateStatus(Long id, String status) {
        LambdaUpdateWrapper<TradeLogDO> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TradeLogDO::getStatus, status);
        updateWrapper.eq(TradeLogDO::getId, id);
        mapper.update(null, updateWrapper);
    }

    @Override
    public BasePage<TradeLogDO> listPage(TradeQuery tradeQuery) {
        return this.listPage(tradeQuery, null);
    }
}
